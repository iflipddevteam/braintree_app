/**
 * Braintree API handler methods to make the Braintree gateway methods available to a client.
 */
;(function() {
	var request = require('request')
	  , stringify = require('json-stringify-safe')
	  , clone = require('clone');
	
	var configBkUser;
	
	var gateway_request = require('./gateway.js').hfn;
	var gateway2_request = require('./gateway2.js').hfn;
	
//	Backendless.initApp(configBk.appId, configBk.javascriptSecretKey, configBk.appVersion);

	// core gateway access routine that can be called by other APIs
	var gateway_auth_fn = function( ) {		
	};
	
	// HTTP Express handler
	var gateway_auth_request = function( req, res ) {
				
		// validate the Backendless options
/*
		// Gateway credentials validated against Backendless instance running the gateway
		if(!req.headers['application-id'] || req.headers['application-id'] !== configBk.appId) {
			res.status(404).send();
			return;			
		};
		
		if(!req.headers['secret-key'] || req.headers['secret-key'] !== configBk.restSecretKey) {
			res.status(404).send();
			return;			
		};
*/
/*
		if(!req.headers['user-token']) {
			res.status(404).send();
			return;			
		};
*/
		if((req.method == 'POST' || req.method == 'PUT') && req.body) {
			var reqBody = JSON.parse(req.body);
			var userToken = reqBody['user-token'];
		}
		if((req.method == 'GET') && req.query) {
			var userToken = req.query['user-token'];
		}
		if (!userToken) {
			res.status(404).send();
			return;			
		};

		// The User user-token is checked against the Backendless instance where user is logged in
		var request_options = {
//			url: 'https://api.backendless.com/' + configBkUser.appVersion + '/users/isvalidusertoken/' + req.headers['user-token'],
			url: 'https://api.backendless.com/' + configBkUser.appVersion + '/users/isvalidusertoken/' + userToken,
			headers: {
				'application-id': configBkUser.appId,
				'secret-key': configBkUser.restSecretKey,
				'application-type': 'REST'
			}
		};
	
		function request_callback( err, response, body ) {
			if(err !== null) {
				res.status(500).send(err);
				return;
			};
			if(!body || typeof body !== 'string' || !(body === 'true')) {
				res.status(401).send();
				return;
			}

			gateway2_request(req, res);
		};
		
		request.get(request_options, request_callback); 
	};	
	
	module.exports.fn = gateway_auth_fn;
	module.exports.hfn = gateway_auth_request;
	module.exports.run = gateway_auth_request;
	
	// Initialization function that returns initialized module.exports structure
	var configurator = function(bku, bt) {
		try {
			if(bku)
				configBkUser = bku;
			else
				configBkUser = require('../config/backendless_user.js');
		}
		catch(ex) {};
		
		if(bt)
			gateway2_request = require('./gateway2.js').config(bt).hfn
		if(bt)
			gateway_request = require('./gateway.js').config(bt).hfn

		return module.exports;
	};
	
	configurator();

	module.exports.config = configurator;

}).call(this);