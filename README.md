# braintree_app
## Overview
This is a minimal implementation of the Braintree v.zero DropIn client for test and illustration purposes
[https://developers.braintreepayments.com/javascript+node/start/overview](Get Started with Braintree).  The demo is implemented two ways for illustrative purposes.  The first way uses explicit APIs `payments/client_token.js` and `payments/payments_methods.js` on the AWS server that communicate with the Braintree gateway.  The second way uses an API `payments/requests.js` that essentially makes all requests supported by the Braintree gateway accessible to the client.


# Use with AWS
Some details:

1. This repo only includes Express handlers for the Braintree gateway.  To use this on AWS, the contents of this repo must be required as a dependency in an integration module (eg. `braintree_run` that loads and configures the handlers.
2.


## Server Backendless Configuration
When this package is used as a dependency in an integration module, the handler Braintree configuration should be passed in when each handler is `require()`'ed, e.g.
```
var btA = 'braintree_app'
  , ping = require(btA + '/monitor/ping.js').run
  , ping_auth = require(btA + '/monitor/ping_auth.js').config(configBkUser).run
```
where `configBkUser` is a Javascript object of the form:
```
{
	appId: *Application ID*,
	javascriptSecretKey: *Javascript Secret Key*,
	restSecretKey: *REST Secret Key*,
	appVersion: "v1"
}
```
The Backendless credentials can be found in Backendless Developers console under **Manage** -> **App Settings** selection for the app.


## Braintree Configuration
When this package is used as a dependency in an integration module, the handler Braintree configuration should be passed in when each handler is `require()`'ed, e.g.
```
var btA = 'braintree_app'
  , gateway_request = require(btA + '/payments/gateway.js').config(configBt).run
  , gateway2_request = require(btA + '/payments/gateway2.js').config(configBt).run
  , gateway_auth_request = require(btA + '/payments/gateway_auth.js').config(configBkUser, configBt).run;	 
```
where `configBt` is a Javascript object of the form:
```
{
    merchantId: *Merchant ID*,
	publicKey: *Public Key*,
	privateKey: *Private Key*
}
```
The Braintree credentials can be found in the [https://sandbox.braintreegateway.com/login](Braintree Dashboard) under "Authorization" item of the **Account** -> **My user** selection.


## Client Backendless Configuration
The `public/index.html` demo page uses the Backendless JavaScript SDK for illustrative purposes.  The HTML code indicates where a call must be made to initialize the SDK.  Example initialization is
```
Backendless.initApp(*Application ID*, *JavaScript Secret Key*, 'v1')
```
The Backendless credentials for a Backendless app can be found in the [develop.backendless.com](Backendless Developer) console under the **Manage** -> **App Settings** selection.

For this demo, it appears the *JavaScript Secret Key* can be set to the null string `''`.

The version string `'v1'` is the specific version of the app for which the credentials apply.


## Braintree Gateway REST API 
This module provides Express handlers that makes the entire Braintree gateway available to a client through a REST API on Backendless in two forms.  The first does not require the user provide any Backendless credentials, the second requiresthat the user be logged into the Backendless User service and provide the Backendless `user-token` for the logged in user.  The Braintree gateway routines are described in the Braintree docs
[https://developers.braintreepayments.com/javascript+node/reference/overview](Braintree Reference Overview)

The URLs for the REST API on Backendless is
```
https://api.backendless.com/<app_id>/v1/files/web/scripts/payments/gateway.js
https://api.backendless.com/<app_id>/v1/files/web/scripts/payments/gateway_auth.js
```

The cURL commands for accessing the REST API have the general form:
```
curl -i -k -H "application-type:REST" \
-H "Content-Type: application/json" \
-d '{ "request":APIrequest, "method":APImethod, ... }' \
-X POST https://api.backendless.com/<application ID>/v1/files/web/scripts/payments/gateway.js

curl -i -k -H "application-type:REST" \
-H "Content-Type: application/json" \
-d '{ "request":"<APIrequest>", "method":"<APImethod>", "params":"[{...}]"}' \
-X POST https://api.backendless.com/<application ID>/v1/files/web/scripts/payments/gateway2.js

curl -i -k -H "application-type:REST" \
-H "Content-Type: application/json" \
-d '{ "user-token":"<user-token>", "request":"<APIrequest>", "method":"<APImethod>", "params":"[{...}]" }' \
-X POST https://api.backendless.com/<application ID>/v1/files/web/scripts/payments/gateway_auth.js
```
where the `...` are the rest of the parameters for the specific gateway request. In the `gateway_auth.js` example, `<user-token>` is the Backendless token for a logged-in user. For example:


The API also support GET requests with query parameters, e.g.
```
curl -i -k -H "application-type:REST" \
 -X GET https://api.backendless.com/<application ID>/v1/files/web/scripts/payments/gateway2.js?request=<APIRequest>&method=<APIMethod>&params=%5B%7B...%7D%5D"

curl -i -k -H "application-type:REST" \
-d '{ "request":APIrequest, "method":APImethod, ... }' \
-X GET https://api.backendless.com/<application ID>/v1/files/web/scripts/payments/gateway_auth.js?user-token=<user-token>&request=<APIRequest>&method=<APIMethod>&params=%5B%7B...%7D%5D"
```
but for now it appears the Backendless Node.js script server does not parse query parameters correctly.

**IMPORTANT NOTE**
For security purposes only the `gateway_auth.js` API should be used in production and direct access to the other two APIs `gateway.js` and `gateway_bk.js` should be disabled. This is easily accomplished by doing the following.  The modules end with the exports:
```
	module.exports.fn = gateway_fn;
	module.exports.hfn = gateway_request;
	module.exports.run = gateway_request;
```
Commenting the out the last export disables direct access to the module, e.g.
```
	module.exports.fn = gateway_fn;
	module.exports.hfn = gateway_request;
//	module.exports.run = gateway_request;
```

## CRON job
The cron job to reconcile the status of transaction info in the Backendless Data service and on Braintree will be implemented as an API in the `cron/` folder similar to the Braintree `payments/gateway.js` API.  This API will use the Braintree Search function, which has a different form than most (all?) of the rest of the Braintree API Requests:
[https://developers.braintreepayments.com/javascript+node/reference/request/transaction/search](https://developers.braintreepayments.com/javascript+node/reference/request/transaction/search)

The cron API will be called by a Backendless Timer on a regular basis like a conventional UNIX cron job:
[http://backendless.com/documentation/business-logic/java/bl_timers.htm](http://backendless.com/documentation/business-logic/java/bl_timers.htm)

It is recommended that a special user be registered with the Backendless App to run the cron job to increase security.

### Overview
The initial anticipated operation of the cron API was:

1.  Search the User table for set of records that have a PaymentGateway table child record with status "SUBMITTED FOR SETTLEMENT"(requires automatic Group By operation) and return objects to depth 2. If any records are returned, for each User record do Steps 2-4:
2.  Extract an array of structures `[{customer_id:  customer_id, transaction_ids: [ ]}, … ]` from the returned records
or just walk them as is. For each structure in the array, or equivalent data in the array of records, do Steps 3-4:
3.  Search Braintree for records with that `customer_id` and the `transaction_id` values with status "SETTLED".
4.  Update the "status" of PaymentGateway records from returned list of transactions.
5.  Repeat Step 1 if the last query returned a full set of records.

The currently proposed operation of the cron API is:

1.  Search the PaymentGateway table for a set of records that have the status "SUBMITTED FOR SETTLEMENT" and return objects to depth 1.  If any `PaymentGateway` records are returned, do Steps 2-5:
2.  Build an structure of objects (because Step 1 will use paging, this structure will have a limited number of key-value members):
```
{ payment_method.customer_id: [ transaction.payment_service_transaction_id, ... ], ... }
```
For each member of the structure do Steps 3-5:
3.  Search Braintree for records with the instance's `customer_id` with status "SETTLED".
4.  Prune the argument array of `transaction.payment_service_transaction_id` values to only those values in the list of transactions returned by Braintree.
5.  Update the `status` of `PaymentGateway` records from pruned list of transactions.
6.  Repeat Step 1 if the last query returned a full set of records.

### Calling the cron job API in Backendless
The cron job API is intended to be activated by a Java Timer in Backendless. 

Assume the URL for the cron job API be the URL:
```
https://api.backendless.com/<application ID>/v1/files/web/scripts/cron/update_status_auth.js
```

The timer code in Backendless under "Business Logic -> Code Blocks -> Timers" section in the Backendless Developer Console.  The documentations about this are here:
[http://backendless.com/documentation/business-logic/java/bl_timers.htm](http://backendless.com/documentation/business-logic/java/bl_timers.htm)

Some basic info here on writing code to send a request to a URL:
[https://docs.oracle.com/javase/tutorial/networking/urls/readingURL.html](https://docs.oracle.com/javase/tutorial/networking/urls/readingURL.html)
[https://docs.oracle.com/javase/7/docs/api/javax/net/ssl/HttpsURLConnection.html](https://docs.oracle.com/javase/7/docs/api/javax/net/ssl/HttpsURLConnection.html)

## Monitor (test) Functions
The app includes some test functions to make it easier to test an installation in the `/monitor` directory.

### Backendless Pings
Several ping functions are provided to test whether the Backendless configuration and `backendless_user.js` are pointing to valid servers.

The other set of pings can be used to test the `backendless_user.js` configuration (the server that hosts the user data tables):
```
curl -i -k -H "application-type:REST"
-H "user-token: <user-token>" \
-X GET https://api.backendless.com/<application ID>/v1/files/web/scripts/monitor/ping_auth.js?user-token=<user-token>&monitor=ping.auth.js

curl -i -k -H "application-type:REST" \
-H "Content-Type:application/json" \
-d '{"user-token":"<user-token>", "monitor":"ping_auth.js"}' \
-X POST https://api.backendless.com/<application ID>/v1/files/web/scripts/monitor/ping_auth.js
```

## Development Note
It turns out the Braintree API:
[https://developers.braintreepayments.com/javascript+node/reference/overview](Braintree Reference Overview)
standardizes API Request parameters in a somewhat unexpected way.  The format appears to be:
```
gateway.*request*.*method*(req1, ..., {key1: value1, ... }, callback);
```
where 

1. `*req1*, ...,` are some number of zero or more scalar parameters that includes any required parameters for the particular request.

2. `{key1: value1, ...}` is an optional object of zero or more scalar parameters that may not exist depending on the particular request.

This makes designing a general API gateway a little more tricky.

The repo contains a second version of the ``payments/gateway2.js`` module that currently is in development to accomodate this variable format.

The Braintree ``gateway.transaction.search()`` API uses a completely different format.  It appears the cron job will have to be coded as a standalone API to accomodate this:
[https://developers.braintreepayments.com/javascript+node/reference/request/transaction/search](https://developers.braintreepayments.com/javascript+node/reference/request/transaction/search)


# Use with [http://www.backendless.com](Backendless.com)
Some details:

1. To use this on Backendless, the contents of this repo, along with `node_modules` folder and a `config/braintree.js` with merchant credentials for the Braintree gateway described below must be put in the `web/scripts` folder of the Backendless Files service.
2. The entire Backendless Files service is by default a single Git repo if one uses that feature, so this repo cannot just be pushed to the Backendless repo.  The entire Backendless Files service contents must first be Fetch'ed from Backendless as a repo, this repo's contents copied into that copy of the Backendless repo locally, and the updated Backendless repo push'ed to Backendless.
3. At this point, the Backendless Users, Roles, and Permissions features are not used in the demo.


## Braintree Configuration
The handlers in `payments/` that access the Braintree gateway `require()` a Braintree configuration file, e.g. `config/braintree.js`.  Following Node.js and npm conventions for now, this folder and package are not included in the repo and must be created in the Backendless Files system.

Example contents of a configuration file are (all properties are required):
```
;(function() {
	module.exports = {
		merchantId: *Merchant ID*,
		publicKey: *Public Key*,
		privateKey: *Private Key*
	}
}).call(this);
```
The Braintree credentials can be found in the [https://sandbox.braintreegateway.com/login](Braintree Dashboard) under "Authorization" item of the **Account** -> **My user** selection.

## Server Backendless Configuration
The secured gateway handler `payments/*_user.js` verifies the current logged in user against an potentially independent  Backendless instance that handles users.  It includes a `require()` for a separate Backendless configuration file, e.g. `config/backendless_user.js`.

Example contents of a configuration file are (all properties are required):
```
;(function() {
	module.exports = {
		appId: *Application ID*,
		javascriptSecretKey: *Javascript Secret Key*,
		restSecretKey: *REST Secret Key*,
		appVersion: "v1"
	}
}).call(this);
```
The Backendless credentials can be found in Backendless Developers console under **Manage** -> **App Settings** selection for the app.

Following Node.js and npm conventions for now, the `config` folder and credential files with these packages are not included in the repo and must be created in the Backendless Files system.



## Transferring to Backendless using Git
The only easy way to upload this app to Backendless Files right now is to put the Files service under Git control

To clone the app onto a local machine and push it to Backendless:

**1. Enable Git in Backendless**

Enabled/Disabled on the **Manage -> App Settings** tab of the Backendless Developer's console.
[http://www.backendless.com/documentation/files/rest/files_git_integration.htm](http://www.backendless.com/documentation/files/rest/files_git_integration.htm)

The URL supposedly is:
   https://git.backendless.com/Application_ID/.git 
BUT some of the Backendless info (and tests) suggests it might be:
  http://git.backendless.com/Application_ID/.git

It's a bit unclear at this moment, but perhaps try https: first in any particular situation and then http:

**2. Clone project repo from Git remote**

Make directory for clone folder
```
  $ cd <source_repo_folder>  (e.g. git)
```
or make directory for clone folder
```
  $ cd <source_repo_parent_folder>
  $ mkdir git
```
Clone the repo
```
  $ git clone git clone https://<username>@bitbucket.org/backfliptech/braintree_app.git
```

**3. Clone Node.js script folder from Backendless.com in Git (Backendless Files Git repo)**

Make directory for clone folder
```
  $ cd <backendless_repo_folder>  (e.g. gitbk)
```
or make directory for clone folder
```
  $ cd <backendless_repo_parent_folder>
  $ mkdir gitbk
```

Clone Files structure including Node.js script folder
```
  $ git clone https://git.backendless.com/Application_ID/.git
  $ cd Application_ID
```

**4. Copy source project to Backendless project**

Copy the entire contents of the cloned source Git repo into the web/scripts folder of the cloned Backendless Git repo.

**5. Install the project in the local Backendless repo**

Build the app in the web/scripts folder of the cloned Backendless Git repo
```
  $ cd web/scripts
  $ npm install
```

**6. Create configuration files in Backendless**

Create the config folder:
```
  $ cd web/scripts
  $ mkdir config
  $ cd config
```

Create a local Braintree credentials file config/braintree.js on Backendless (uploaded on git push)
```
  /**
   * Example Braintree API sandbox credentials returned as a JS object
   */
  ;(function() {
    module.exports = {
      merchantId: "<merchant ID>",
      publicKey: "<public Key>",
      privateKey: "<private Key>"
    }
  }).call(this);
```

Create a local Backendless credentials file config/backendless.js for the Braintree gateway on Backendless
```
  /**
   * Example Backendless account API credentials returned as a JS object
   */
  ;(function() {
	module.exports = {
		appId: "<Application ID>",
		javascriptSecretKey: "<Javascript Secret Key>",
		restSecretKey: "<REST Secret Key>",
		appVersion: "v1"
    }
  }).call(this);
```

Create a local Backendless credentials file config/backendless_user.js for the Backendless instance that manages user logins on Backendless
```
  /**
   * Example Backendless account API credentials returned as a JS object
   */
  ;(function() {
	module.exports = {
		appId: "<Application ID>",
		javascriptSecretKey: "<Javascript Secret Key>",
		restSecretKey: "<REST Secret Key>",
		appVersion: "v1"
    }
  }).call(this);
```

**7. Push Node.js script folder to Backendless.com in Git**

Create a .gitignore file, e.g.
```
  **/.DS_Store
  .DS_Store
  .settings
  .project
  web/scripts/public/assets/*
  web/scripts/default_env.sh
  web/scripts/curls.sh
  web/scripts/ngrok2.sh
  web/scripts/cert/*
```

One must first commit files to the local Git archive using eGit, OR 

Commit files using Git (make sure one is in the Git repo folder,  not the web/scripts or lower level folder in the Git repo folder)
```
  $ git add file.txt (add new single file to repo)  OR $ git add *  (add all new files)
  $ git commit -m 'Initial commit with new file'
```

Push local repo to Backendless in Git
```
  $ git push -u origin master
```

**8. Register Braintree hosts with Backendless App**

In the **Manage -> App Settings** menu of the app, under **External Hosts in Business Logic**, enter the Braintree hosts:
```
api.sandbox.braintreegateway.com
api.braintreegateway.com

```

